import numpy as np
import pandas as pd
from numba import jit

def neutralize(alpha):
    return alpha.sub(alpha.mean(axis=1),axis=0)

def group_neutralize(weights,groups):
    # weights - pd.DataFrame (index=times,columns=tickers)
    # groups - pd.Series (index=tickers)
    
    dictionary = {}
    n_w = pd.DataFrame()
    for i in groups.unique():
        sec_tic = groups[groups==i].index
        dictionary[i] = sec_tic
        n_w = pd.concat([n_w, neutralize(weights[dictionary[i]])], axis=1)
    return n_w

def normalize(alpha):
    return alpha.div(alpha.abs().sum(axis=1).replace(0,1),axis=0)

def truncate(alpha,max_weight):
    return alpha.where(alpha.abs().sub(max_weight*alpha.abs().sum(axis=1),axis=0)<0,max_weight*np.sign(alpha))

@jit(nopython=True)
def rank_last(a):
    if not np.isfinite(a[-1]):
        return np.nan
    qt = (a>a[-1]).sum()
    lt = (a<a[-1]).sum()
    eq = (a==a[-1]).sum()
    n = qt+lt+eq
    if n == 0:
        return np.nan
    elif n == 1:
        return 0.5
    else:
        return (2*lt+eq-1)/(n-1)/2

@jit(nopython=True)
def jit_ts_rank(m,n):
    res = np.repeat(np.nan,m.shape[0]*m.shape[1]).reshape(*m.shape)
    for i in range(n-1,m.shape[0]):
        for j in range(m.shape[1]):
            res[i,j] = rank_last(m[i-n+1:i+1,j])
    return res

def ts_rank(df,n):
    return pd.DataFrame(jit_ts_rank(df.values,n),index=df.index,columns=df.columns)

def ts_zscore(df,n):
    return (df-df.rolling(n).mean())/df.rolling(n).std()

def ts_corr(df1,df2,n):
    return ((df1*df2).rolling(n).mean() - df1.rolling(n).mean()*df2.rolling(n).mean()) / np.sqrt( ((df1*df1).rolling(n).mean() - df1.rolling(n).mean()**2) * ((df2*df2).rolling(n).mean() - df2.rolling(n).mean()**2) )

