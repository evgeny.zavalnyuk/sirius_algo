import os
import sys
import json
import requests
import numpy as np
import pandas as pd
from pytz import UTC
from datetime import timedelta
from datetime import datetime
from dateutil.parser import parse

# ========================================================================================================================= #
BINANCE_START_DATE = '2017-07-01'

BINANCE_SPOT_API_URL = 'https://www.binance.com/api/v3/'
BINANCE_SPOT_API_DEPTH_LIMIT = 5000
BINANCE_SPOT_API_AGGTRADES_LIMIT = 1000
BINANCE_SPOT_API_KLINES_LIMIT = 1000

BINANCE_FUTURE_API_URL = 'https://www.binance.com/fapi/v1/'
BINANCE_FUTURE_API_KLINES_LIMIT = 1500

# ========================================================================================================================= #
# Utils
def stdout(s):
    sys.stdout.write(s)
    sys.stdout.flush()

def parse_datetime(s):
    dt = parse(s)
    dt = dt.astimezone(UTC) if dt.tzinfo else UTC.localize(dt)
    
    return dt

# ========================================================================================================================= #
def request_spot_symbols():
    url = os.path.join(BINANCE_SPOT_API_URL,'exchangeInfo')
    R = requests.get(url)
    D = R.json()
    
    return pd.DataFrame(D['symbols'])

def filter_symbols(symbols,quote_asset):
    return symbols[symbols['quoteAsset']==quote_asset]

def request_spot_tickers(quote_asset):
    return filter_symbols(request_spot_symbols(),quote_asset).baseAsset.tolist()

def request_spot_orderbook(base_asset,quote_asset):
    R = requests.get(
        url = os.path.join(BINANCE_SPOT_API_URL,'/depth'),
        params = dict(
            symbol = base_asset+quote_asset,
            limit = BINANCE_SPOT_API_DEPTH_LIMIT
        )
    )
    D = R.json()
    
    return dict(
        bids = pd.DataFrame(D['bids'],columns=['price','volume']).astype(float).set_index('price').volume,
        asks = pd.DataFrame(D['asks'],columns=['price','volume']).astype(float).set_index('price').volume
    )

def request_spot_trades(base_asset,quote_asset,start_dt,end_dt):
    # start_dt and end_dt are human-readable datetimes of str type
    start_ts, end_ts = parse_datetime(start_dt).timestamp(), parse_datetime(end_dt).timestamp()
    
    D = []
    start_ts_iter = int(np.floor(1000*start_ts))
    end_ts_iter = min(start_ts_iter+1000*60*60,int(np.ceil(1000*end_ts)))
    while not len(D) and start_ts_iter < end_ts_iter:
        R = requests.get(
            url = os.path.join(BINANCE_SPOT_API_URL,'aggTrades'),
            params = dict(
                symbol = base_asset+quote_asset,
                startTime = start_ts_iter,
                endTime   = end_ts_iter,
                limit = BINANCE_SPOT_API_AGGTRADES_LIMIT
            )
        )
        D = R.json()
        start_ts_iter = end_ts_iter+1
        end_ts_iter = min(start_ts_iter+1000*60*60,int(np.ceil(1000*end_ts)))
    
    if not len(D):
        return pd.DataFrame(columns=['id','price','volume','t','isSell']).astype({'id':int,'price':float,'volume':float,'t':int,'isSell':bool})
    
    last_item = D[-1]
    last_id = last_item['a']
    while last_item['T']/1000 < end_ts:
        R = requests.get(
            url = os.path.join(BINANCE_SPOT_API_URL,'aggTrades'),
            params = dict(
                symbol = base_asset+quote_asset,
                fromId = last_id+1,
                limit = BINANCE_SPOT_API_AGGTRADES_LIMIT
            )
        )
        D_iter = R.json()
        D += D_iter
        if len(D_iter) < BINANCE_SPOT_API_AGGTRADES_LIMIT:
            break
        last_item = D_iter[-1]
        last_id = last_item['a']
        
    trades = pd.DataFrame(D).drop(['M','f','l'],axis=1).astype({'p':float,'q':float}).rename(columns={'a':'id','p':'price','q':'volume','m':'isSell','T':'t'})
    trades = trades[(trades['t']/1000>=start_ts)&(trades['t']/1000<end_ts)]
    
    return trades[['id','price','volume','t','isSell']]

def request_spot_candles(base_asset,quote_asset,interval,start_dt,end_dt):
    # interval : from 1s,1m,3m,5m,15m,30m,1h,2h,4h,6h,8h,12h,1d,3d,1w,1M
    # start_dt and end_dt are human-readable datetimes of str type
    start_ts, end_ts = parse_datetime(start_dt).timestamp(), parse_datetime(end_dt).timestamp()
    
    R = requests.get(
        url = os.path.join(BINANCE_SPOT_API_URL,'klines'),
        params = dict(
            symbol = base_asset+quote_asset,
            interval = interval,
            startTime = int(np.floor(1000*start_ts)),
            endTime = int(np.ceil(1000*end_ts)),
            limit = BINANCE_SPOT_API_KLINES_LIMIT
        )
    )
    D = R.json()
    
    if not len(D):
        return pd.DataFrame(columns=['start_t','end_t','open','high','low','close','volume','quote_volume','buy_volume','buy_quote_volume','n_trades']).astype({'start_t':int,'end_t':int,'open':float,'high':float,'low':float,'close':float,'volume':float,'quote_volume':float,'buy_volume':float,'buy_quote_volume':float,'n_trades':int})
    
    last_item = D[-1]
    while last_item[0]/1000 < end_ts:
        R = requests.get(
            url = os.path.join(BINANCE_SPOT_API_URL,'klines'),
            params = dict(
                symbol = base_asset+quote_asset,
                interval = interval,
                startTime = last_item[6]+1,
                endTime = int(np.ceil(1000*end_ts)),
                limit = BINANCE_SPOT_API_KLINES_LIMIT
            )
        )
        D_iter = R.json()
        D += D_iter
        if len(D_iter) < BINANCE_SPOT_API_KLINES_LIMIT:
            break
        last_item = D_iter[-1]
    
    candles = pd.DataFrame(D,columns=['start_t','open','high','low','close','volume','end_t','quote_volume','n_trades','buy_volume','buy_quote_volume',11]).drop(11,axis=1).astype({'open':float,'high':float,'low':float,'close':float,'volume':float,'quote_volume':float,'buy_volume':float,'buy_quote_volume':float})
    candles = candles[(candles['start_t']/1000>=start_ts)&(candles['start_t']/1000<end_ts)]
    
    return candles[['start_t','end_t','open','high','low','close','volume','quote_volume','buy_volume','buy_quote_volume','n_trades']]

def request_future_candles(base_asset,quote_asset,contract_type,interval,start_dt,end_dt):
    # interval : from 1s,1m,3m,5m,15m,30m,1h,2h,4h,6h,8h,12h,1d,3d,1w,1M
    # start_dt and end_dt are human-readable datetimes of str type
    start_ts, end_ts = parse_datetime(start_dt).timestamp(), parse_datetime(end_dt).timestamp()
    
    R = requests.get(
        url = os.path.join(BINANCE_FUTURE_API_URL,'continuousKlines'),
        params = dict(
            pair = base_asset+quote_asset,
            contractType = contract_type,
            interval = interval,
            startTime = int(np.floor(1000*start_ts)),
            endTime = int(np.ceil(1000*end_ts)),
            limit = BINANCE_FUTURE_API_KLINES_LIMIT
        )
    )
    D = R.json()
    
    if not len(D):
        return pd.DataFrame(columns=['start_t','end_t','open','high','low','close','volume','quote_volume','buy_volume','buy_quote_volume','n_trades']).astype({'start_t':int,'end_t':int,'open':float,'high':float,'low':float,'close':float,'volume':float,'quote_volume':float,'buy_volume':float,'buy_quote_volume':float,'n_trades':int})
    
    last_item = D[-1]
    while last_item[0]/1000 < end_ts:
        R = requests.get(
            url = os.path.join(BINANCE_FUTURE_API_URL,'continuousKlines'),
            params = dict(
                pair = base_asset+quote_asset,
                contractType = contract_type,
                interval = interval,
                startTime = last_item[6]+1,
                endTime = int(np.ceil(1000*end_ts)),
                limit = BINANCE_FUTURE_API_KLINES_LIMIT
            )
        )
        D_iter = R.json()
        D += D_iter
        if len(D_iter) < BINANCE_FUTURE_API_KLINES_LIMIT:
            break
        last_item = D_iter[-1]
    
    candles = pd.DataFrame(D,columns=['start_t','open','high','low','close','volume','end_t','quote_volume','n_trades','buy_volume','buy_quote_volume',11]).drop(11,axis=1).astype({'open':float,'high':float,'low':float,'close':float,'volume':float,'quote_volume':float,'buy_volume':float,'buy_quote_volume':float})
    candles = candles[(candles['start_t']/1000>=start_ts)&(candles['start_t']/1000<end_ts)]
    
    return candles[['start_t','end_t','open','high','low','close','volume','quote_volume','buy_volume','buy_quote_volume','n_trades']]

# ========================================================================================================================= #
def dump_spot_candles(base_assets,quote_asset,interval,start_dt,end_dt,data_dir):
    # start_dt and end_dt are human-readable datetimes of str type
    
    if not os.path.exists(data_dir):
        os.makedirs(data_dir)
    
    for i,base_asset in enumerate(base_assets):
        if os.path.exists(os.path.join(data_dir,f'{base_asset}-{quote_asset}.parquet')):
            continue
        stdout(f'{i+1}/{len(base_assets)} {base_asset}')
        candles = request_spot_candles(base_asset,quote_asset,interval,start_dt,end_dt)
        candles.to_parquet(os.path.join(data_dir,f'{base_asset}-{quote_asset}.parquet'),engine='pyarrow',compression='brotli')
        stdout(' : done\n')

def dump_future_candles(base_assets,quote_asset,contract_type,interval,start_dt,end_dt,data_dir):
    # start_dt and end_dt are human-readable datetimes of str type
    
    if not os.path.exists(data_dir):
        os.makedirs(data_dir)
    
    for base_asset in base_assets:
        if os.path.exists(os.path.join(data_dir,f'{base_asset}-{quote_asset}.parquet')):
            continue
        stdout(f'{base_asset}')
        candles = request_future_candles(base_asset,quote_asset,contract_type,interval,start_dt,end_dt)
        candles.to_parquet(os.path.join(data_dir,f'{base_asset}-{quote_asset}.parquet'),engine='pyarrow',compression='brotli')
        stdout(' : done\n')
   